#!/bin/bash

hostname=$(awk -F "=" '/mattermost_hostname/ {print $2}' config.ini)
channel=$(awk -F "=" '/mattermost_channel/ {print $2}' config.ini)
api_path=$(awk -F "=" '/mattermost_path/ {print $2}' config.ini)
icon=$(awk -F "=" '/mattermost_icon_url/ {print $2}' config.ini)

report=$(docker run --rm membership_report | awk '{printf "%s\\n", $0}')

readarray -td\& sections <<< "$report"; declare -p sections;

if [ ! -z "${report}" ]; then
  for section in "${sections[@]}"
  do
    payload="{ \"channel\": \"$channel\", \"username\": \"Membership Report\", \"icon_url\": \"$icon\", \"text\": \"$section\" }"
    curl -X POST --data-urlencode "payload=$payload" \
      https://$hostname/$api_path
  done
fi
