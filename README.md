# Install

```
$ sudo -s
# mkdir -f /opt
# cd /opt
# git clone https://gitlab.com/ballarat-hackerspace/services/membership_report.git
# cd membership_report
# cp config.ini.example config.ini
# [ edit config.ini with your values ]
```

# Build

```
$ docker build -t membership_report .
```

# Test: Manually generate a report

```
$ docker run --rm membership_report
```

# Automate

```
# systemctl enable /opt/membership_report/membership_report.service
# systemctl enable /opt/membership_report/membership_report.timer
# systemctl start membership_report.timer
```
