#!/usr/bin/env python3
import requests
import datetime
import calendar
import configparser
from datetime import date
from operator import itemgetter
from dateutil import parser

config = configparser.ConfigParser()
config.read('config.ini')

today = date.today()
days_in_month = calendar.monthrange(today.year, today.month)[1]

# all_members[member_name] = [ { membership_details }, { membership_details }, ... ]
all_members = {}

def get_users_name(id, auth):
    try:
        r = requests.get(f'https://api.tidyhq.com/v1/contacts/{id}', params=auth)
        return(f'{r.json()["last_name"].strip()}, {r.json()["first_name"].strip()}'[:30])
    except:
        return(f'Missing name: {id}')

def add_member(store, name, data):
    name = name.lower()
    try:
        store[name].append(data)
    except:
        store[name]=[]
        store[name].append(data)

# ===============================================================================
# TidyHQ
#

payload = { 'domain_prefix': 'default',
            'client_id': config['tidyhq']['client_id'],
            'client_secret': config['tidyhq']['client_secret'],
            'username': config['tidyhq']['username'],
            'password': config['tidyhq']['password'],
            'grant_type': 'password' }

# Get an access_token
r = requests.post(f'https://{config["tidyhq"]["domain"]}.tidyhq.com/oauth/token', params=payload)
access_token = r.json()['access_token']

payload = { 'access_token': access_token }

# Get Membership Levels
r = requests.get('https://api.tidyhq.com/v1/membership_levels', params=payload)
membership_levels = r.json()

# Get Memberships
r = requests.get('https://api.tidyhq.com/v1/memberships', params=payload)
memberships = r.json()

# process members
for member in memberships:
    try:
        expire = datetime.datetime.strptime(member['end_date'], '%Y-%m-%dT%H:%M:%S%z')
        member['days'] = abs((expire.date()-today).days)
    except:
        expire = today
        member['days'] = 0
    member['formatted_name'] = get_users_name(member["contact_id"], payload)
    try:
        member['member_active'] = (member["state"] != 'expired')
    except:
        member['member_active'] = False
    member['level'] = next(item for item in membership_levels if item["id"] == member['membership_level_id'])['name']
    member['source'] = "tidyhq"
    add_member(all_members, member['formatted_name'], member)

# ===============================================================================
# Patreon
#

patreon_access_token = config['patreon']['access_token']
patreons = []

headers = { 'Authorization': f'Bearer {patreon_access_token}' }
payload = { 'fields[member]': 'full_name,is_follower,email,last_charge_date,last_charge_status,lifetime_support_cents,patron_status,currently_entitled_amount_cents,pledge_relationship_start,will_pay_amount_cents',
            'fields[tier]': 'title',
            'fields[user]': 'full_name,hide_pledges' }
r = requests.get('https://www.patreon.com/api/oauth2/v2/campaigns', headers=headers)
campaigns = r.json()
for campaign in campaigns['data']:
    id = campaign['id']
    q = requests.get(f'https://www.patreon.com/api/oauth2/v2/campaigns/{id}/members', headers=headers)
    members = q.json()
    for member in members['data']:
        p = requests.get(f'https://www.patreon.com/api/oauth2/v2/members/{member["id"]}', headers=headers, params=payload)
        patreons.append(p.json()['data']['attributes'])

# process members
for patreon in patreons:
    if patreon['last_charge_date']:
        patreon['since'] = parser.parse(patreon['last_charge_date'])
        patreon['days'] = abs(days_in_month - abs((patreon['since'].date()-today).days))
    else:
        patreon['days'] = 0

    n = patreon['full_name'].split(' ', 1)
    try:
        # test for missing lastname
        if n[1].strip() != "":
            patreon['formatted_name'] = f'{n[1].strip()}, {n[0].strip()}'
        else:
            patreon['formatted_name'] = f'{n[0].strip()}'
    except:
        patreon['formatted_name'] = patreon['full_name']
    patreon['member_active'] = (patreon['patron_status'] == 'active_patron')
    if patreon['will_pay_amount_cents'] < 2300:
        patreon['level'] = 'Learner Member'
    elif patreon['will_pay_amount_cents'] < 4600:
        patreon['level'] = 'Social Member'
    elif patreon['will_pay_amount_cents'] < 17000:
        patreon['level'] = 'Full Time Member'
    else:
        patreon['level'] = 'Corporate Sponsor'
    patreon['level_dollars'] = f'${patreon["will_pay_amount_cents"]/100:2.2f}'
    patreon['source'] = "patreon"
    add_member(all_members, patreon['formatted_name'], patreon)

# ===============================================================================
# Create the report
#

active_members = {}
expired_members = {}
for member in all_members:
    active = False
    oldest_day = 180
    for membership in all_members[member]:
        if membership['member_active']:
            active=True
        else:
            if membership['days'] < oldest_day:
                oldest_day = membership['days']
    if active:
        # only grab active plan memberships, skip old expired ones
        active_members[member] = [x for x in all_members[member] if x['member_active']]
    else:
        # only show members with recently expired plans
        if oldest_day < 180:
            expired_members[member] = all_members[member]

print(f'*Active Members: {len(active_members):3d}*')
print('```')

membership_plans = {}

for member in sorted(active_members.keys()):
    n = f'{member.upper()}'
    if len(active_members[member]) == 1:
        n = n + ' ─'
    else:
        n = n + ' ┬'

    for i in range(len(active_members[member])):
        m = active_members[member][i]
        try:
            membership_plans[m['level']]+=1
        except KeyError:
            membership_plans[m['level']]=1
        print(f'{n:>30s} {m["level"][:20]:20s} via {m["source"]:7s} ─ {m["days"]:4d} days ago')
        if (i + 2) == len(active_members[member]):
            n = '└'
        else:
            n = '├'
print('```')

print('&')

# Seeders are also Association Members
if 'Seeder' in membership_plans:
    if 'Association Member' in membership_plans:
        membership_plans['Association Member'] += membership_plans['Seeder']
    else:
        membership_plans['Association Member'] = membership_plans['Seeder']


print(f'*Expired Members: {len(expired_members):3d}*')
print('```')

for member in sorted(expired_members.keys()):
    n = f'{member.upper()}'

    # minimise records to 1 per membership level
    membership_types = {}
    for i in range(len(expired_members[member])):
        if expired_members[member][i]['level'] not in membership_types:
            membership_types[expired_members[member][i]['level']] = {
                    'source': expired_members[member][i]['source'],
                    'days': expired_members[member][i]['days']
                    }

    if len(membership_types) == 1:
        n = n + ' ─'
    else:
        n = n + ' ┬'

    for i in membership_types:
        if i != list(membership_types.keys())[0]:
            if i == list(membership_types.keys())[-1]:
                n = '└'
            else:
                n = '├'
        print(f'{n:>30s} {i[:20]:20s} via {membership_types[i]["source"]:7s} ─ {membership_types[i]["days"]:4d} days ago')
print('```')

print('&')

print('*SUMMARY*')
print('```')

print(f'{"Current active members":>30s} ─ {len(active_members):3d}', end='')
print(f'{"All time number of members":>30s} ─ {len(all_members):3d}')
print(f'{"Recently expired members":>30s} ─ {len(expired_members):3d}')
print()

c = 1
for plan in sorted(membership_plans.keys()):
    print(f'{plan[:29].strip()+"s":>30s} ─ {membership_plans[plan]:3d}', end='' if (c%2) else None)
    c+=1
print('', end='' if (c%2) else None)

print('```')
